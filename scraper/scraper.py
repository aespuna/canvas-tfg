import argparse
import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import config


def build_driver():
    fp = webdriver.FirefoxProfile()
    fp.add_extension(config.PLUGIN_FILE_PATH)
    # To allow connections from https to our localhost
    fp.set_preference("security.mixed_content.block_active_content", False)
    caps = DesiredCapabilities.FIREFOX
    return webdriver.Firefox(firefox_profile=fp, capabilities=caps)


def visit_site(driver, rank, site):
    print('site %d: %s' % (rank, site))
    driver.get('http://' + site)
    # Stay some time in the page
    time.sleep(10)


parser = argparse.ArgumentParser(description='Scrap websites in a list')
parser.add_argument('start', type=int, default=0, help='Start index (0 indexed)')
parser.add_argument('end', type=int, help='End index (not included)', nargs='?')
parser.add_argument('-f', dest='filename', type=str, default='data/top-1m.csv.zip',
                    help='File containing one domain per line or an alexa csv. Can be a zip or gz file')


def main():
    args = parser.parse_args()
    sites = config.load_alexa_csv(args.filename)[slice(args.start, args.end, None)]

    driver = build_driver()
    driver.set_page_load_timeout(25)

    for rank, site in enumerate(sites, args.start + 1):
        try:
            visit_site(driver, rank, site)
        except TimeoutException:
            print('timed out')


if __name__ == '__main__':
    main()
