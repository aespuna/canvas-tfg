# show index
# receive form
# start scrapper on task
import json
import time
from datetime import datetime
from urllib.parse import urlparse, urlunparse

import os
import redis
from bottle import route, run, request, static_file
from celery import Celery
from hashlib import md5
from jinja2 import Environment, FileSystemLoader, escape, Markup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import config
from web.utils import utc_now

celery = Celery('web.public', backend='redis://', broker='redis://')


def add_schema(url):
    parsed = urlparse(url, scheme='http')
    if not parsed.netloc:
        parsed = urlparse('//' + url, scheme='http')
    return urlunparse(parsed)


class Dummy(object):
    def set_page_load_timeout(self, *args, **kwargs):
        time.sleep(5)

    def get(self, *args, **kwargs):
        time.sleep(5)

    def save_screenshot(self, *args, **kwargs):
        time.sleep(5)


def build_driver():
    fp = webdriver.FirefoxProfile()
    fp.add_extension(config.PLUGIN_FILE_PATH)
    # To allow connections from https to our localhost
    fp.set_preference("security.mixed_content.block_active_content", False)
    caps = DesiredCapabilities.FIREFOX
    return webdriver.Firefox(firefox_profile=fp, capabilities=caps)


LOADING_BROWSER = 'LOADING_BROWSER'
INJECTING_ID = 'INJECTING_ID'
COLLECTING_DATA = 'COLLECTING_DATA'
TAKING_SCREENSHOT = 'TAKING_SCREENSHOT'


@celery.task(name='web.public.scrap', bind=True)
def scrap(self, url, identifier):
    image_path = os.path.join('img', identifier[:2], identifier[2:4], '%s.png' % identifier[4:])
    redis_conn.set(identifier, json.dumps({'url': url,
                                           'capture': '/' + image_path,
                                           'timestamp': utc_now().timestamp(),
                                           'scripts': []}))
    self.update_state(state='PROGRESS', meta={'url': url})
    browser = build_driver()
    browser.set_page_load_timeout(20)
    self.update_state(state='PROGRESS', meta={'status': 'Loading browser...', 'url': url})
    browser.get('http://localhost:9000/start/' + identifier)
    self.update_state(state='PROGRESS', meta={'status': 'Injecting identifier...', 'url': url})

    try:
        browser.get(url)
        self.update_state(state='PROGRESS', meta={'status': 'Collecting data...', 'url': url})
        time.sleep(5)
    except TimeoutException:
        pass
    self.update_state(state='PROGRESS', meta={'status': 'Taking screenshot...', 'url': url})
    path = os.path.join(config.PROJECT_DIR, 'web', 'img', identifier[:2], identifier[2:4])
    os.makedirs(path, mode=0o755, exist_ok=True)
    browser.save_screenshot(os.path.join(path, '%s.png' % identifier[4:]))
    browser.quit()
    return json.loads(redis_conn.get(identifier).decode('utf8'))


def highlight(text, word='toDataURL', style='color:red'):
    position = text.find(word)
    if position >= 0:
        end = position + len(word)
        return (escape(text[:position]) +
                Markup('<span style="%s">%s</span>' % (style, text[position:end])) +
                escape(text[end:]))
    return text


_POOL = redis.ConnectionPool(**config.REDIS_PARAMS)
redis_conn = redis.StrictRedis(connection_pool=_POOL)
TEMPLATE_DIR = os.path.join(config.PROJECT_DIR, 'web', 'templates')
jinja_env = Environment(loader=FileSystemLoader(TEMPLATE_DIR))
jinja_env.filters['highlight'] = highlight
jinja_env.filters['to_time'] = lambda x: datetime.utcfromtimestamp(x).strftime('%Y-%m-%dT%H:%M:%SZ')
index_template = jinja_env.get_template('index.html')
result_template = jinja_env.get_template('result.html')


def identifier_from_url(url):
    return md5(url.encode('utf8')).hexdigest()


@route('/', ['GET'])
def index():
    return index_template.render()


@route('/s', ['GET'])
def search():
    url = request.query.get('q')
    url = add_schema(url)
    identifier = identifier_from_url(url)
    # if not args force:
    cached_result = redis_conn.get(identifier)
    if cached_result:
        return index_template.render(result=json.loads(cached_result.decode('utf8')))
    else:
        t = scrap.apply_async(args=[url, identifier])
        return index_template.render(task_id=t.id, url=url)


@route('/t/<task_id>', ['GET'])
def task_status(task_id):
    t = scrap.AsyncResult(task_id)
    state_case = {
        'PENDING': lambda x: {'html': 'Waiting...', 'finished': False},
        'PROGRESS': lambda x: {'html': x.info.get('status', ''), 'finished': False},
        'SUCCESS': lambda x: {'html': result_template.render(result=x.info), 'finished': True},
    }
    return json.dumps(state_case.get(t.status, lambda x: 'Error')(t))


@route('/img/<path:path>', ['GET'])
def serve_image(path):
    return static_file(path, root=os.path.join(config.PROJECT_DIR, 'web', 'img'))


def main():
    run(host='0.0.0.0', port=8080, server='gunicorn', workers=4)


if __name__ == '__main__':
    main()
