var self = require("sdk/self");
var pageMod = require("sdk/page-mod");

pageMod.PageMod({
    include: "*",
    contentScriptWhen: "start",
    contentScriptFile: self.data.url("canvas-detect.js"),
});
